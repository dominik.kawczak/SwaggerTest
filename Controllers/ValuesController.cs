﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SwaggerTest.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [HttpPut("{id}")]
        public void Put(int id, IFormFile file)
        {
        }

        [HttpPost]
        public void Post(Person person)
        {
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
